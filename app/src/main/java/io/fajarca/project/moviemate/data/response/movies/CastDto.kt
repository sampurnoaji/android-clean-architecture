package io.fajarca.project.moviemate.data.response.movies

import com.squareup.moshi.Json

data class CastDto(
    @field:Json(name = "cast")
    val casts: List<Cast?> = emptyList()
) {
    data class Cast(
        @field:Json(name = "cast_id")
        val castId: Int? = null,
        @field:Json(name = "character")
        val character: String? = null,
        @field:Json(name = "name")
        val name: String? = null,
        @field:Json(name = "order")
        val order: Int? = null,
        @field:Json(name = "profile_path")
        val profilePath: String? = null
    )
}
