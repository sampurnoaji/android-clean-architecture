package io.fajarca.project.moviemate.presentation.screen.moviedetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetailData
import io.fajarca.project.moviemate.domain.usecase.moviedetail.GetMovieCastsUseCase
import io.fajarca.project.moviemate.domain.usecase.moviedetail.GetMovieDetailUseCase
import io.fajarca.project.moviemate.domain.usecase.moviedetail.GetMovieImagesUseCase
import io.fajarca.project.moviemate.domain.usecase.moviedetail.GetMovieVideosUseCase
import io.fajarca.project.moviemate.domain.usecase.moviedetail.GetSimilarMovieUseCase
import javax.inject.Inject
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MovieDetailViewModel @Inject constructor(
    private val getMovieDetailUseCase: GetMovieDetailUseCase,
    private val getMovieCastsUseCase: GetMovieCastsUseCase,
    private val getMovieImagesUseCase: GetMovieImagesUseCase,
    private val getSimilarMovieUseCase: GetSimilarMovieUseCase,
    private val getMovieVideosUseCase: GetMovieVideosUseCase
) : ViewModel() {

    private val _movie = MutableLiveData<Result<MovieDetailData>>()
    val movie: LiveData<Result<MovieDetailData>>
        get() = _movie

    fun getMovieDetail(movieId: Int) {
        _movie.value = Result.Loading
        viewModelScope.launch {
            val movieDetailDeferred = async { getMovieDetailUseCase(movieId) }
            val castsDeferred = async { getMovieCastsUseCase(movieId) }
            val imagesDeferred = async { getMovieImagesUseCase(movieId) }
            val similarDeferred = async { getSimilarMovieUseCase(movieId) }
            val videosDeferred = async { getMovieVideosUseCase(movieId) }

            val detail = movieDetailDeferred.await() ?: return@launch
            val casts = castsDeferred.await()
            val images = imagesDeferred.await()
            val similar = similarDeferred.await()
            val videos = videosDeferred.await()

            val data = MovieDetailData(detail, casts, images, similar, videos)
            _movie.value = Result.Success(data)
        }
    }
}
