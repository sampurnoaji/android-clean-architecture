package io.fajarca.project.moviemate.domain.entity.moviedetail

data class SimilarMovie(
    val id: Int,
    val posterPath: String,
    val title: String,
    val voteAverage: Float,
    val adult: Boolean
)
