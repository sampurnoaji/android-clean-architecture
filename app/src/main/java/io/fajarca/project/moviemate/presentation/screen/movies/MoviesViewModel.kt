package io.fajarca.project.moviemate.presentation.screen.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.movielist.Banner
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.entity.movielist.MovieSection
import io.fajarca.project.moviemate.domain.usecase.movielist.GetBannerUseCase
import io.fajarca.project.moviemate.domain.usecase.movielist.GetNowPlayingMoviesUseCase
import io.fajarca.project.moviemate.domain.usecase.movielist.GetPopularMoviesUseCase
import io.fajarca.project.moviemate.domain.usecase.movielist.GetTopRatedMoviesUseCase
import javax.inject.Inject
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MoviesViewModel @Inject constructor(
    private val getNowPlayingMoviesUseCase: GetNowPlayingMoviesUseCase,
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase,
    private val getTopRatedMoviesUseCase: GetTopRatedMoviesUseCase,
    private val getBannerUseCase: GetBannerUseCase
) : ViewModel() {

    private val _movies = MutableLiveData<Result<List<MovieSection>>>()
    val movies: LiveData<Result<List<MovieSection>>>
        get() = _movies

    private val _banner = MutableLiveData<Result<Banner>>()
    val banner: LiveData<Result<Banner>>
        get() = _banner

    fun getMovies() = runBlocking {
        _movies.value = Result.Loading

        viewModelScope.launch {
            val combinedMovies = mutableListOf<MovieSection>()

            val nowPlaying = async { getNowPlayingMoviesUseCase(UseCase.None()) }
            val popular = async { getPopularMoviesUseCase(UseCase.None()) }
            val topRated = async { getTopRatedMoviesUseCase(UseCase.None()) }

            val nowPlayingMovies = nowPlaying.await()

            combinedMovies.add(
                MovieSection(
                    "Now Playing",
                    nowPlayingMovies
                )
            )

            combinedMovies.add(
                MovieSection(
                    "Popular",
                    popular.await()
                )
            )
            combinedMovies.add(
                MovieSection(
                    "Top Rated",
                    topRated.await()
                )
            )

            getNowPlaying(nowPlayingMovies)
            _movies.value = Result.Success(combinedMovies)
        }
    }

    private fun getNowPlaying(nowPlayingMovies: List<Movie>) {
        _banner.value = getBannerUseCase(nowPlayingMovies)
    }
}
