package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.SimilarMoviesDto
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovie
import javax.inject.Inject

class SimilarMovieMapper @Inject constructor() : Mapper<SimilarMoviesDto, List<SimilarMovie>>() {
    override fun map(input: SimilarMoviesDto): List<SimilarMovie> {
        return input.results?.map {
            SimilarMovie(
                it?.id ?: 0,
                it?.posterPath ?: "",
                it?.title ?: "",
                it?.voteAverage ?: 0f,
                it?.adult ?: false
            )
        } ?: emptyList()
    }
}
