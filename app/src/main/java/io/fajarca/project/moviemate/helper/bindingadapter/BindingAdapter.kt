package io.fajarca.project.moviemate.helper.bindingadapter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.fajarca.project.moviemate.data.vo.MovieConstant

@BindingAdapter("loadImage")
fun loadPortraitImage(view: ImageView, imageUrl: String?) {
    if (imageUrl.isNullOrEmpty()) return

    val url = MovieConstant.IMAGE_BASE_URL_POSTER + imageUrl

    Glide.with(view.context)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(0.2f)
        .placeholder(ColorDrawable(Color.LTGRAY))
        .into(view)
}

@BindingAdapter("loadBackdrop")
fun loadBackdrop(view: ImageView, imageUrl: String?) {
    if (imageUrl.isNullOrEmpty()) return

    val url = MovieConstant.IMAGE_BASE_URL_BACKDROP + imageUrl

    Glide.with(view.context)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .thumbnail(0.2f)
        .placeholder(ColorDrawable(Color.LTGRAY))
        .into(view)
}
