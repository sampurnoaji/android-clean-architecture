package io.fajarca.project.moviemate.domain.usecase.search

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.search.MediaType
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.repository.SearchRepository
import javax.inject.Inject

class SearchUseCase @Inject constructor(private val repository: SearchRepository) {

    suspend operator fun invoke(query: String, includeAdult: Boolean): Result<List<SearchResultUiModel>> {
        val apiResult = repository.multiSearch(query, includeAdult)
        return when (apiResult) {
            is Result.Success -> {
                return if (apiResult.data.isEmpty()) {
                    Result.Empty
                } else {
                    Result.Success(handleData(apiResult.data))
                }
            }
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    private fun handleData(input: List<SearchResult>): List<SearchResultUiModel> {
        val searchResults = mutableListOf<SearchResultUiModel>()
        input.forEach { searchResult ->
            when (searchResult.mediaType) {
                "person" -> searchResults.add(createUiModel(searchResult))
                "movie" -> searchResults.add(createUiModel(searchResult))
                "tv" -> searchResults.add(createUiModel(searchResult))
            }
        }
        return searchResults
    }

    private fun createUiModel(searchResult: SearchResult): SearchResultUiModel {
        return when (searchResult.mediaType) {
            "person" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.PERSON,
                searchResult.name,
                searchResult.profilePath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            "movie" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.MOVIE,
                searchResult.title,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            "tv" -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.SERIES,
                searchResult.name,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
            else -> SearchResultUiModel(
                searchResult.adult,
                searchResult.id,
                MediaType.MOVIE,
                searchResult.title,
                searchResult.posterPath,
                searchResult.voteAverage,
                searchResult.releaseDate
            )
        }
    }
}
