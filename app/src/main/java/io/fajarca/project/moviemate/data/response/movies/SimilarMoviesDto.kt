package io.fajarca.project.moviemate.data.response.movies

import com.squareup.moshi.Json

data class SimilarMoviesDto(
    @field:Json(name = "results")
    val results: List<Result?>? = null
) {
    data class Result(
        @field:Json(name = "adult")
        val adult: Boolean? = null,
        @field:Json(name = "id")
        val id: Int? = null,
        @field:Json(name = "poster_path")
        val posterPath: String? = null,
        @field:Json(name = "title")
        val title: String? = null,
        @field:Json(name = "vote_average")
        val voteAverage: Float? = null
    )
}
