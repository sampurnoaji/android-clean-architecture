package io.fajarca.project.moviemate.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.fajarca.project.moviemate.presentation.MainActivity

@Module
abstract class ActivityInjectionModule {

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity
}
