package io.fajarca.project.moviemate.domain.factory

import android.view.View
import android.view.ViewGroup
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.domain.entity.moviedetail.CastSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImageSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideosSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovieSection
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.entity.movielist.MovieSection
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.domain.entity.serieslist.SeriesSection

interface ItemTypeFactory {
    fun type(movieSection: MovieSection): Int
    fun type(movie: Movie): Int
    fun type(seriesSection: SeriesSection): Int
    fun type(series: Series): Int
    fun type(searchResult: SearchResultUiModel): Int
    fun type(genre: MovieDetail.Genre): Int
    fun type(castSection: CastSection): Int
    fun type(movieImageSection: MovieImageSection): Int
    fun type(similarMovieSection: SimilarMovieSection): Int
    fun type(movieVideosSection: MovieVideosSection): Int
    fun createViewHolder(parent: View, viewGroup: ViewGroup, type: Int): BaseViewHolder<*>
}
