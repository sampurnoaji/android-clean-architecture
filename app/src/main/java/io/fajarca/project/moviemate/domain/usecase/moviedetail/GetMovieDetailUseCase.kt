package io.fajarca.project.moviemate.domain.usecase.moviedetail

import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetailUiModel
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import io.fajarca.project.moviemate.helper.extension.toLocalDate
import javax.inject.Inject

class GetMovieDetailUseCase @Inject constructor(private val repository: MovieRepository) : UseCase<MovieDetailUiModel?, Int>() {

    override suspend fun invoke(params: Int): MovieDetailUiModel? {
        val apiResult = repository.getMovieDetail(params)
        return when (apiResult) {
            is Result.Success -> mapData(apiResult.data)
            else -> null
        }
    }

    private fun mapData(input: MovieDetail): MovieDetailUiModel {
        return MovieDetailUiModel(
            input.id,
            input.title,
            input.originalTitle,
            input.originalLanguage,
            input.overview,
            input.posterPath,
            input.backdropPath,
            input.voteCount,
            input.voteAverage,
            input.popularity,
            transformReleaseData(input.releaseDate),
            input.adult,
            transformRuntime(input.runtime),
            input.tagline,
            input.status,
            input.genres
        )
    }

    private fun transformReleaseData(releaseDate: String): String {
        return releaseDate.toLocalDate("yyyy-MM-dd", "MMMM yyyy")
    }

    private fun transformRuntime(runtime: Int): String {
        val hours = runtime / 60
        val minutes = runtime % 60
        return "${hours}h ${minutes}m"
    }
}
