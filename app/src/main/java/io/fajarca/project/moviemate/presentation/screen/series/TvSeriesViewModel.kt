package io.fajarca.project.moviemate.presentation.screen.series

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.serieslist.SeriesSection
import io.fajarca.project.moviemate.domain.usecase.tvserieslist.GetOnTheAirSeriesUseCase
import io.fajarca.project.moviemate.domain.usecase.tvserieslist.GetPopularSeriesUseCase
import io.fajarca.project.moviemate.domain.usecase.tvserieslist.GetTopRatedSeriesUseCase
import javax.inject.Inject
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class TvSeriesViewModel @Inject constructor(
    private val getPopularSeriesUseCase: GetPopularSeriesUseCase,
    private val getTopRatedSeriesUseCase: GetTopRatedSeriesUseCase,
    private val getOnTheAirSeriesUseCase: GetOnTheAirSeriesUseCase
) : ViewModel() {

    private val _series = MutableLiveData<Result<List<SeriesSection>>>()
    val movies: LiveData<Result<List<SeriesSection>>>
        get() = _series

    fun getSeries() = runBlocking {
        _series.value = Result.Loading

        viewModelScope.launch {
            val combinedSeries = mutableListOf<SeriesSection>()

            val topRated = async { getTopRatedSeriesUseCase(UseCase.None()) }
            val popular = async { getPopularSeriesUseCase(UseCase.None()) }
            val onTheAir = async { getOnTheAirSeriesUseCase(UseCase.None()) }

            combinedSeries.add(SeriesSection("Most Popular TV", popular.await()))
            combinedSeries.add(SeriesSection("Top Rated TV Shows", topRated.await()))
            combinedSeries.add(SeriesSection("Currently On The Air", onTheAir.await()))

            _series.value = Result.Success(combinedSeries)
        }
    }
}
