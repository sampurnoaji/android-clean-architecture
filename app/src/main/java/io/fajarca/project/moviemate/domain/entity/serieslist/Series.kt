package io.fajarca.project.moviemate.domain.entity.serieslist

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.domain.factory.ItemTypeFactory

data class Series(
    val id: Int,
    val name: String,
    val originalName: String,
    val overview: String,
    val posterPath: String,
    val backdropPath: String,
    val voteCount: Int,
    val voteAverage: Float,
    val popularity: Float,
    val firstAirDate: String
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
