package io.fajarca.project.moviemate.presentation.screen.search

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.abstraction.BaseRecyclerViewAdapter
import io.fajarca.project.moviemate.data.factory.ItemTypeFactoryImpl
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.databinding.FragmentSearchBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import io.fajarca.project.moviemate.helper.extension.gone
import io.fajarca.project.moviemate.helper.extension.setMarginTop
import io.fajarca.project.moviemate.helper.extension.visible
import io.fajarca.project.moviemate.presentation.utils.DebouncingTextListener
import io.fajarca.project.moviemate.presentation.utils.RecyclerviewDividerItemDecoration

class SearchFragment : BaseFragment<FragmentSearchBinding, SearchViewModel>(),
    ItemClickListener {

    override fun getLayoutResourceId() = R.layout.fragment_search
    override fun getViewModelClass() = SearchViewModel::class.java
    private val adapter by lazy { BaseRecyclerViewAdapter(ItemTypeFactoryImpl(), arrayListOf(), this) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        initRecyclerView()
        observeSearchResult()
        applyTopInset()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val searchMenuItem = menu.findItem(R.id.action_search)
        searchMenuItem.expandActionView()
        val searchManager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchView = searchMenuItem.actionView as SearchView
        searchView.queryHint = "Search movie, series, cast..."
        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        searchView.setOnQueryTextListener(DebouncingTextListener(700, lifecycleScope) { query ->
            vm.search(query, true)
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun applyTopInset() {
        val listener = object : OnApplyWindowInsetsListener {
            override fun onApplyWindowInsets(v: View?, insets: WindowInsetsCompat?): WindowInsetsCompat? {
                binding.appBarLayout.setMarginTop(insets?.systemWindowInsetTop ?: 0)
                return insets?.consumeSystemWindowInsets()
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(binding.root, listener)
    }

    private fun setupToolbar() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (requireActivity() as AppCompatActivity).supportActionBar?.title = null
    }

    private fun observeSearchResult() {
        vm.searchResults.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    IdlingResourceWrapper.increment()
                    binding.recyclerView.gone()
                    binding.progressBar.visible()
                    binding.tvEmptySearchResultNotice.gone()
                }
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.visible()
                    adapter.refreshItems(it.data)
                    binding.tvEmptySearchResultNotice.gone()
                }
                is Result.Empty -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.gone()
                    binding.tvEmptySearchResultNotice.visible()
                }
                is Result.Error -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.gone()
                    binding.tvEmptySearchResultNotice.gone()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.addItemDecoration(RecyclerviewDividerItemDecoration(requireActivity()))
        binding.recyclerView.adapter = adapter
    }

    override fun onClick(item: BaseItemModel) {
        val movie = item as SearchResultUiModel
        findNavController().navigate(SearchFragmentDirections.actionSearchFragmentToFragmentMovieDetail(movie.id))
    }
}
