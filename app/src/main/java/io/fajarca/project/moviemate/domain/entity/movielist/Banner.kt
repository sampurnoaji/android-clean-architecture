package io.fajarca.project.moviemate.domain.entity.movielist

data class Banner(
    val id: Int,
    val title: String,
    val overview: String,
    val imageUrl: String
)
