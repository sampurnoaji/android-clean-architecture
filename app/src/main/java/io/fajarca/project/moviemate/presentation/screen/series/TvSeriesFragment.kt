package io.fajarca.project.moviemate.presentation.screen.series

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.OnApplyWindowInsetsListener
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.abstraction.BaseRecyclerViewAdapter
import io.fajarca.project.moviemate.data.factory.ItemTypeFactoryImpl
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.databinding.FragmentTvSeriesBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import io.fajarca.project.moviemate.helper.extension.gone
import io.fajarca.project.moviemate.helper.extension.setMarginTop
import io.fajarca.project.moviemate.helper.extension.visible

class TvSeriesFragment : BaseFragment<FragmentTvSeriesBinding, TvSeriesViewModel>(), ItemClickListener {

    override fun getLayoutResourceId() = R.layout.fragment_tv_series
    override fun getViewModelClass() = TvSeriesViewModel::class.java
    private val adapter by lazy {
        BaseRecyclerViewAdapter(
            ItemTypeFactoryImpl(),
            arrayListOf(),
            this
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setupToolbar()
        applyTopInset()
        observeTvSeriesResult()
        vm.getSeries()
    }

    private fun setupToolbar() {
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbar)
        (requireActivity() as AppCompatActivity).supportActionBar?.title = "TV Series"
    }

    private fun applyTopInset() {
        val listener = object : OnApplyWindowInsetsListener {
            override fun onApplyWindowInsets(v: View?, insets: WindowInsetsCompat?): WindowInsetsCompat? {
                binding.appBarLayout.setMarginTop(insets?.systemWindowInsetTop ?: 0)
                return insets?.consumeSystemWindowInsets()
            }
        }

        ViewCompat.setOnApplyWindowInsetsListener(binding.root, listener)
    }

    private fun observeTvSeriesResult() {
        vm.movies.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    IdlingResourceWrapper.increment()
                    binding.recyclerView.gone()
                    binding.progressBar.visible()
                }
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.visible()
                    adapter.refreshItems(it.data)
                }
                is Result.Error -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.gone()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    override fun onClick(item: BaseItemModel) {
        val series = item as Series
    }
}
