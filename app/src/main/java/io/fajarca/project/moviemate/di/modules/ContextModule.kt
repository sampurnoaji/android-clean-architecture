package io.fajarca.project.moviemate.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import io.fajarca.project.moviemate.MovieMateApp
import javax.inject.Singleton

@Module
class ContextModule(val app: MovieMateApp) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }
}
