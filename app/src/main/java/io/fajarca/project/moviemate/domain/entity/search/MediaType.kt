package io.fajarca.project.moviemate.domain.entity.search

enum class MediaType {
    MOVIE,
    SERIES,
    PERSON
}
