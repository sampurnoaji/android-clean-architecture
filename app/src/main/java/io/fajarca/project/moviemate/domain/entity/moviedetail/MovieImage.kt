package io.fajarca.project.moviemate.domain.entity.moviedetail

data class MovieImage(
    val aspectRatio: Float,
    val filePath: String,
    val height: Int,
    val width: Int
)
