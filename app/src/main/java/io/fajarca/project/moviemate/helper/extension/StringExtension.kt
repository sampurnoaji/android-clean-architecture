package io.fajarca.project.moviemate.helper.extension

import io.fajarca.project.moviemate.data.vo.MovieConstant
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone

fun String.toLocalDate(inFormat: String, outFormat: String): String {
    if (this.isEmpty()) return this

    return try {
        val inputFormat = SimpleDateFormat(inFormat, Locale.US)
        inputFormat.timeZone = TimeZone.getTimeZone("GMT")

        val outputFormat = SimpleDateFormat(outFormat, Locale.getDefault())
        val date = inputFormat.parse(this) ?: return ""
        outputFormat.format(date)
    } catch (e: Exception) {
        throw IllegalArgumentException("Not a valid datetime format")
    }
}

fun String.createImageUrl() = MovieConstant.IMAGE_BASE_URL_POSTER + this
