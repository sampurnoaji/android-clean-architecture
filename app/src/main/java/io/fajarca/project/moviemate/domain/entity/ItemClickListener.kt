package io.fajarca.project.moviemate.domain.entity

import io.fajarca.project.moviemate.abstraction.BaseItemModel

interface ItemClickListener {
    fun onClick(item: BaseItemModel)
}
