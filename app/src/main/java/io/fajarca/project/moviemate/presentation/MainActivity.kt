package io.fajarca.project.moviemate.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.helper.extension.gone
import io.fajarca.project.moviemate.helper.extension.makeStatusBarTransparent
import io.fajarca.project.moviemate.helper.extension.visible
import javax.inject.Inject
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    private val navController by lazy { Navigation.findNavController(this, R.id.navHostFragment) }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigation()
        makeStatusBarTransparent()
    }

    private fun setupNavigation() {
        bottomNavigationView.setupWithNavController(navController)
    }

    fun showBottomNavigation() {
        bottomNavigationView.visible()
    }

    fun hideBottomNavigation() {
        bottomNavigationView.gone()
    }
}
