package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.MovieVideosDto
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideo
import javax.inject.Inject

class MovieVideosMapper @Inject constructor() : Mapper<MovieVideosDto, List<MovieVideo>>() {

    override fun map(input: MovieVideosDto): List<MovieVideo> {
        return input.results?.map {
            MovieVideo(it?.id ?: "", it?.key ?: "", it?.name ?: "", it?.site ?: "", it?.size ?: 0, it?.type ?: "")
        } ?: emptyList()
    }
}
