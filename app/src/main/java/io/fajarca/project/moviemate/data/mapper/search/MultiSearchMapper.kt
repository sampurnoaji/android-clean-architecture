package io.fajarca.project.moviemate.data.mapper.search

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.search.MultiSearchDto
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import javax.inject.Inject

class MultiSearchMapper @Inject constructor() : Mapper<MultiSearchDto, List<SearchResult>>() {

    override fun map(input: MultiSearchDto): List<SearchResult> {
        return input.results.map {
            SearchResult(
                it?.adult ?: false,
                it?.firstAirDate ?: "",
                it?.id ?: 0,
                it?.mediaType ?: "",
                it?.name ?: "",
                it?.title ?: "",
                it?.posterPath ?: "0f",
                it?.releaseDate ?: "",
                it?.voteAverage ?: 0.0f,
                it?.profilePath ?: ""
            )
        }
    }
}
