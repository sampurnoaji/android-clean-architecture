package io.fajarca.project.moviemate.domain.entity.moviedetail

data class MovieVideo(
    val id: String,
    val key: String,
    val name: String,
    val site: String,
    val size: Int,
    val type: String
)
