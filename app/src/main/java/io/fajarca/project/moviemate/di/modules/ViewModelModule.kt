package io.fajarca.project.moviemate.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.fajarca.project.moviemate.di.annotation.ViewModelKey
import io.fajarca.project.moviemate.di.factory.ViewModelFactory
import io.fajarca.project.moviemate.presentation.screen.moviedetail.MovieDetailViewModel
import io.fajarca.project.moviemate.presentation.screen.movies.MoviesViewModel
import io.fajarca.project.moviemate.presentation.screen.search.SearchViewModel
import io.fajarca.project.moviemate.presentation.screen.series.TvSeriesViewModel

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    abstract fun providesMoviesViewModel(viewModel: MoviesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun providesMovieDetailViewModel(viewModel: MovieDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TvSeriesViewModel::class)
    abstract fun providesTvSeriesViewModel(viewModel: TvSeriesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun providesSearchViewModel(viewModel: SearchViewModel): ViewModel
}
