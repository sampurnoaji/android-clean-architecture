package io.fajarca.project.moviemate.domain.repository

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.search.SearchResult

interface SearchRepository {
    suspend fun multiSearch(query: String, includeAdult: Boolean): Result<List<SearchResult>>
}
