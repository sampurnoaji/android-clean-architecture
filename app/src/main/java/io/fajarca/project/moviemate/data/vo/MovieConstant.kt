package io.fajarca.project.moviemate.data.vo

object MovieConstant {
    const val IMAGE_BASE_URL_POSTER = "https://image.tmdb.org/t/p/w185/"
    const val IMAGE_BASE_URL_BACKDROP = "https://image.tmdb.org/t/p/w1280"
}
