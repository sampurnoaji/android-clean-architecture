package io.fajarca.project.moviemate.di.modules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.fajarca.project.moviemate.presentation.screen.moviedetail.MovieDetailFragment
import io.fajarca.project.moviemate.presentation.screen.movies.MoviesFragment
import io.fajarca.project.moviemate.presentation.screen.search.SearchFragment
import io.fajarca.project.moviemate.presentation.screen.series.TvSeriesFragment

@Module
abstract class FragmentInjectionModule {
    @ContributesAndroidInjector
    abstract fun contributesHomeFragment(): MoviesFragment

    @ContributesAndroidInjector
    abstract fun contributesMovieDetailFragment(): MovieDetailFragment

    @ContributesAndroidInjector
    abstract fun contributesTvSeriesFragment(): TvSeriesFragment

    @ContributesAndroidInjector
    abstract fun contributesSearchFragment(): SearchFragment
}
