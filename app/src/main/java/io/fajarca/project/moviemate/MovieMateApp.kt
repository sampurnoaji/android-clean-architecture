package io.fajarca.project.moviemate

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.fajarca.project.moviemate.di.component.AppComponent
import io.fajarca.project.moviemate.di.component.DaggerAppComponent
import io.fajarca.project.moviemate.di.modules.ContextModule
import io.fajarca.project.moviemate.di.modules.NetworkModule
import javax.inject.Inject
import timber.log.Timber

open class MovieMateApp : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        initAppDependencyInjection()
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initAppDependencyInjection() {
        appComponent = DaggerAppComponent
            .builder()
            .contextModule(ContextModule(this))
            .networkModule(NetworkModule())
            .build()

        appComponent.inject(this)
    }

    open fun getComponent(): AppComponent {
        return appComponent
    }
}
