package io.fajarca.project.moviemate.domain.entity.moviedetail

import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.domain.factory.ItemTypeFactory

data class CastSection(val header: String, val casts: List<Cast>) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}
