package io.fajarca.project.moviemate.abstraction

import io.fajarca.project.moviemate.domain.factory.ItemTypeFactory

abstract class BaseItemModel {
    abstract fun type(itemTypeFactory: ItemTypeFactory): Int
}
