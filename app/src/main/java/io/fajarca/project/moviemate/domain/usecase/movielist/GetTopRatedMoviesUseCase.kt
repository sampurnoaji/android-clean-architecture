package io.fajarca.project.moviemate.domain.usecase.movielist

import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.repository.MovieRepository
import javax.inject.Inject

class GetTopRatedMoviesUseCase @Inject constructor(private val repository: MovieRepository) : UseCase<List<Movie>, UseCase.None>() {

    override suspend operator fun invoke(params: UseCase.None): List<Movie> {
        val result = repository.getTopRatedMovies()
        return when (result) {
            is Result.Success -> result.data
            else -> emptyList()
        }
    }
}
