package io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemMovieVideoBinding
import io.fajarca.project.moviemate.databinding.ItemMovieVideoContainerBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideo
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideosSection

class MovieVideosViewHolder(private val binding: ItemMovieVideoContainerBinding) : BaseViewHolder<MovieVideosSection>(binding) {

    private lateinit var adapter: MovieVideosAdapter
    companion object {
        const val LAYOUT = R.layout.item_movie_video
    }

    override fun bind(item: MovieVideosSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = MovieVideosAdapter(item.videos, clickListener)
        binding.recyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.isNestedScrollingEnabled = false
        binding.executePendingBindings()
    }

    class MovieVideosAdapter(private val items: List<MovieVideo>, private val clickListener: ItemClickListener) : RecyclerView.Adapter<MovieVideosAdapter.MovieVideoViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieVideoViewHolder {
            return MovieVideoViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: MovieVideoViewHolder, position: Int) {
            holder.bind(items[position], position, clickListener)
        }

        class MovieVideoViewHolder(private val binding: ItemMovieVideoBinding) : RecyclerView.ViewHolder(binding.root) {

            fun bind(video: MovieVideo, position: Int, clickListener: ItemClickListener) {
                binding.video = video

                Glide.with(binding.ivMovie.context)
                    .load("https://img.youtube.com/vi/${video.key}/mqdefault.jpg")
                    .error(ContextCompat.getDrawable(binding.ivMovie.context, R.drawable.ic_broken_image))
                    .placeholder(ColorDrawable(Color.LTGRAY))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .thumbnail(0.1f)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(binding.ivMovie)

                binding.executePendingBindings()
            }

            companion object {
                fun create(viewGroup: ViewGroup): MovieVideoViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemMovieVideoBinding.inflate(layoutInflater, viewGroup, false)
                    return MovieVideoViewHolder(binding)
                }
            }
        }
    }
}
