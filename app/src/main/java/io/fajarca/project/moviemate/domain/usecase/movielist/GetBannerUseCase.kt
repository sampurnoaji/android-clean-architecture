package io.fajarca.project.moviemate.domain.usecase.movielist

import io.fajarca.project.moviemate.data.vo.MovieConstant.IMAGE_BASE_URL_POSTER
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.movielist.Banner
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import javax.inject.Inject

class GetBannerUseCase @Inject constructor() {

    operator fun invoke(nowPlayingMovies: List<Movie>): Result<Banner> {
        if (nowPlayingMovies.isNotEmpty()) {
            val movie = nowPlayingMovies[0]
            return Result.Success(Banner(movie.id, movie.title, movie.overview, handleImageUrl(movie.posterPath)))
        }
        return Result.Empty
    }

    private fun handleImageUrl(posterPath: String): String {
        return IMAGE_BASE_URL_POSTER + posterPath
    }
}
