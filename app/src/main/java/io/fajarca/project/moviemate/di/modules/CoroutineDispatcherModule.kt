package io.fajarca.project.moviemate.di.modules

import dagger.Binds
import dagger.Module
import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.dispatcher.DispatcherProvider

@Module
interface CoroutineDispatcherModule {
    @Binds
    fun bindDispatcher(dispatcherProvider: CoroutineDispatcherProvider): DispatcherProvider
}
