package io.fajarca.project.moviemate.domain.repository

import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.moviedetail.Cast
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImage
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideo
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovie
import io.fajarca.project.moviemate.domain.entity.movielist.Movie

interface MovieRepository {
    suspend fun getNowPlayingMovies(): Result<List<Movie>>
    suspend fun getMovieDetail(movieId: Int): Result<MovieDetail>
    suspend fun getPopularMovies(): Result<List<Movie>>
    suspend fun getTopRatedMovies(): Result<List<Movie>>
    suspend fun getMovieCasts(movieId: Int): Result<List<Cast>>
    suspend fun getMovieImages(movieId: Int): Result<List<MovieImage>>
    suspend fun getSimilarMovies(movieId: Int): Result<List<SimilarMovie>>
    suspend fun getMovieVideos(movieId: Int): Result<List<MovieVideo>>
}
