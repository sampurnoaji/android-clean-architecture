package io.fajarca.project.moviemate.data.mapper.movie

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.movies.MovieDetailDto
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import javax.inject.Inject

class MovieDetailMapper @Inject constructor() : Mapper<MovieDetailDto, MovieDetail>() {

    override fun map(input: MovieDetailDto): MovieDetail {
        return MovieDetail(
            input.id ?: 0,
            input.title ?: "",
            input.originalTitle ?: "",
            input.originalLanguage ?: "",
            input.overview ?: "",
            input.posterPath ?: "",
            input.backdropPath ?: "",
            input.voteCount ?: 0,
            input.voteAverage ?: 0.0f,
            input.popularity ?: 0.0f,
            input.releaseDate ?: "",
            input.adult ?: false,
            input.runtime ?: 0,
            input.tagline ?: "",
            input.status ?: "",
            mapGenres(input.genres)
        )
    }

    private fun mapGenres(input: List<MovieDetailDto.Genre?>?): List<MovieDetail.Genre> {
        val genres = mutableListOf<MovieDetail.Genre>()
        input?.forEach {
            genres.add(MovieDetail.Genre(it?.id ?: 0, it?.name ?: ""))
        }
        return genres
    }
}
