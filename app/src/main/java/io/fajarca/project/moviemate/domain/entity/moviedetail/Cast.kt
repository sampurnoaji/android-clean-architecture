package io.fajarca.project.moviemate.domain.entity.moviedetail

data class Cast(
    val id: Int,
    val character: String,
    val name: String,
    val profileImageUrl: String,
    val order: Int
)
