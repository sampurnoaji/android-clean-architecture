package io.fajarca.project.moviemate.presentation.screen.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.usecase.search.SearchUseCase
import javax.inject.Inject
import kotlinx.coroutines.launch

class SearchViewModel @Inject constructor(
    private val searchUseCase: SearchUseCase
) : ViewModel() {

    private val _searchResults = MutableLiveData<Result<List<SearchResultUiModel>>>()
    val searchResults: LiveData<Result<List<SearchResultUiModel>>>
        get() = _searchResults

    fun search(query: String, includeAdult: Boolean) {
        _searchResults.value = Result.Loading
        viewModelScope.launch {
            _searchResults.value = searchUseCase(query, includeAdult)
        }
    }
}
