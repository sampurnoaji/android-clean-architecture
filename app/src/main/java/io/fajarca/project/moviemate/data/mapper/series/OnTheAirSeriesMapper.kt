package io.fajarca.project.moviemate.data.mapper.series

import io.fajarca.project.moviemate.abstraction.Mapper
import io.fajarca.project.moviemate.data.response.series.OnTheAirDto
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import javax.inject.Inject

class OnTheAirSeriesMapper @Inject constructor() : Mapper<OnTheAirDto, List<Series>>() {

    override fun map(input: OnTheAirDto): List<Series> {
        val series = mutableListOf<Series>()
        input.results.forEach {
            series.add(
                Series(
                    it?.id ?: 0,
                    it?.name ?: "",
                    it?.originalName ?: "",
                    it?.overview ?: "",
                    it?.posterPath ?: "",
                    it?.backdropPath ?: "",
                    it?.voteCount ?: 0,
                    it?.voteAverage ?: 0.0f,
                    it?.popularity ?: 0.0f,
                    it?.firstAirDate ?: ""
                )
            )
        }
        return series
    }
}
