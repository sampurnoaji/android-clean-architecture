package io.fajarca.project.moviemate.data.repository

import io.fajarca.project.moviemate.data.dispatcher.CoroutineDispatcherProvider
import io.fajarca.project.moviemate.data.mapper.search.MultiSearchMapper
import io.fajarca.project.moviemate.data.source.SearchRemoteDataSource
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.search.SearchResult
import io.fajarca.project.moviemate.domain.repository.SearchRepository
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(
    private val dispatcherProvider: CoroutineDispatcherProvider,
    private val multiSearchMapper: MultiSearchMapper,
    private val remoteDataSource: SearchRemoteDataSource
) : SearchRepository {

    override suspend fun multiSearch(query: String, includeAdult: Boolean): Result<List<SearchResult>> {
        val apiResult = remoteDataSource.multiSearch(dispatcherProvider.io, query, includeAdult)
        return when (apiResult) {
            is Result.Success -> Result.Success(multiSearchMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }
}
