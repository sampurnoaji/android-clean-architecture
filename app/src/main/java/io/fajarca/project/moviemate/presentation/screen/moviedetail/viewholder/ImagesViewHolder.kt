package io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemImageBinding
import io.fajarca.project.moviemate.databinding.ItemImageContainerBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImage
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImageSection

class ImagesViewHolder(private var binding: ItemImageContainerBinding) : BaseViewHolder<MovieImageSection>(binding) {

    private lateinit var adapter: MovieImagesAdapter
    companion object {
        const val LAYOUT = R.layout.item_image
    }

    override fun bind(item: MovieImageSection, clickListener: ItemClickListener) {
        binding.tvHeader.text = item.header
        adapter = MovieImagesAdapter(item.images, clickListener)
        binding.recyclerView.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.isNestedScrollingEnabled = false
        binding.executePendingBindings()
    }

    class MovieImagesAdapter(private val items: List<MovieImage>, private val clickListener: ItemClickListener) : RecyclerView.Adapter<MovieImagesAdapter.MovieImagesViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieImagesViewHolder {
            return MovieImagesViewHolder.create(parent)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: MovieImagesViewHolder, position: Int) {
            holder.bind(items[position], position, clickListener)
        }

        class MovieImagesViewHolder(private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {

            fun bind(movieImage: MovieImage, position: Int, clickListener: ItemClickListener) {
                binding.image = movieImage
                binding.executePendingBindings()
            }

            companion object {
                fun create(viewGroup: ViewGroup): MovieImagesViewHolder {
                    val layoutInflater = LayoutInflater.from(viewGroup.context)
                    val binding = ItemImageBinding.inflate(layoutInflater, viewGroup, false)
                    return MovieImagesViewHolder(binding)
                }
            }
        }
    }
}
