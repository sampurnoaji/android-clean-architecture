package io.fajarca.project.moviemate.data.factory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.fajarca.project.moviemate.abstraction.BaseViewHolder
import io.fajarca.project.moviemate.databinding.ItemCastContainerBinding
import io.fajarca.project.moviemate.databinding.ItemGenreBinding
import io.fajarca.project.moviemate.databinding.ItemImageContainerBinding
import io.fajarca.project.moviemate.databinding.ItemMovieVideoContainerBinding
import io.fajarca.project.moviemate.databinding.ItemMoviesBinding
import io.fajarca.project.moviemate.databinding.ItemSearchResultBinding
import io.fajarca.project.moviemate.databinding.ItemSeriesListBinding
import io.fajarca.project.moviemate.databinding.ItemSimilarContainerBinding
import io.fajarca.project.moviemate.domain.entity.moviedetail.CastSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieDetail
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieImageSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.MovieVideosSection
import io.fajarca.project.moviemate.domain.entity.moviedetail.SimilarMovieSection
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.domain.entity.movielist.MovieSection
import io.fajarca.project.moviemate.domain.entity.search.SearchResultUiModel
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.domain.entity.serieslist.SeriesSection
import io.fajarca.project.moviemate.domain.factory.ItemTypeFactory
import io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder.CastViewHolder
import io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder.GenreViewHolder
import io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder.ImagesViewHolder
import io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder.MovieVideosViewHolder
import io.fajarca.project.moviemate.presentation.screen.moviedetail.viewholder.SimilarViewHolder
import io.fajarca.project.moviemate.presentation.screen.movies.MovieViewHolder
import io.fajarca.project.moviemate.presentation.screen.search.SearchResultViewHolder
import io.fajarca.project.moviemate.presentation.screen.series.SeriesViewHolder

class ItemTypeFactoryImpl : ItemTypeFactory {

    override fun type(movieSection: MovieSection): Int {
        return MovieViewHolder.LAYOUT
    }

    override fun type(movie: Movie): Int {
        return MovieViewHolder.LAYOUT
    }

    override fun type(seriesSection: SeriesSection): Int {
        return SeriesViewHolder.LAYOUT
    }

    override fun type(series: Series): Int {
        return SeriesViewHolder.LAYOUT
    }

    override fun type(searchResult: SearchResultUiModel): Int {
        return SearchResultViewHolder.LAYOUT
    }

    override fun type(genre: MovieDetail.Genre): Int {
        return GenreViewHolder.LAYOUT
    }

    override fun type(castSection: CastSection): Int {
        return CastViewHolder.LAYOUT
    }

    override fun type(movieImageSection: MovieImageSection): Int {
        return ImagesViewHolder.LAYOUT
    }

    override fun type(similarMovieSection: SimilarMovieSection): Int {
        return SimilarViewHolder.LAYOUT
    }

    override fun type(movieVideosSection: MovieVideosSection): Int {
        return MovieVideosViewHolder.LAYOUT
    }

    override fun createViewHolder(parent: View, viewGroup: ViewGroup, type: Int): BaseViewHolder<*> {
        return when (type) {
            MovieViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemMoviesBinding.inflate(layoutInflater, viewGroup, false)
                MovieViewHolder(binding)
            }
            SeriesViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSeriesListBinding.inflate(layoutInflater, viewGroup, false)
                SeriesViewHolder(binding)
            }
            CastViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemCastContainerBinding.inflate(layoutInflater, viewGroup, false)
                CastViewHolder(binding)
            }
            SearchResultViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSearchResultBinding.inflate(layoutInflater, viewGroup, false)
                SearchResultViewHolder(binding)
            }
            GenreViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemGenreBinding.inflate(layoutInflater, viewGroup, false)
                GenreViewHolder(binding)
            }
            ImagesViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemImageContainerBinding.inflate(layoutInflater, viewGroup, false)
                ImagesViewHolder(binding)
            }
            SimilarViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemSimilarContainerBinding.inflate(layoutInflater, viewGroup, false)
                SimilarViewHolder(binding)
            }
            MovieVideosViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemMovieVideoContainerBinding.inflate(layoutInflater, viewGroup, false)
                MovieVideosViewHolder(binding)
            }
            else -> throw IllegalArgumentException("Unknown type")
        }
    }
}
