package io.fajarca.project.moviemate.di.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import io.fajarca.project.moviemate.MovieMateApp
import io.fajarca.project.moviemate.di.modules.ActivityInjectionModule
import io.fajarca.project.moviemate.di.modules.ContextModule
import io.fajarca.project.moviemate.di.modules.CoroutineDispatcherModule
import io.fajarca.project.moviemate.di.modules.FragmentInjectionModule
import io.fajarca.project.moviemate.di.modules.NetworkModule
import io.fajarca.project.moviemate.di.modules.RepositoryModule
import io.fajarca.project.moviemate.di.modules.SharedPreferenceModule
import io.fajarca.project.moviemate.di.modules.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityInjectionModule::class,
        FragmentInjectionModule::class,
        ContextModule::class,
        NetworkModule::class,
        SharedPreferenceModule::class,
        CoroutineDispatcherModule::class,
        ViewModelModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent {
    fun inject(instance: MovieMateApp?)
}
