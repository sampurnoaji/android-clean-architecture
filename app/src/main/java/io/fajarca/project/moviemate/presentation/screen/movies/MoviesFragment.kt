package io.fajarca.project.moviemate.presentation.screen.movies

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.abstraction.BaseFragment
import io.fajarca.project.moviemate.abstraction.BaseItemModel
import io.fajarca.project.moviemate.abstraction.BaseRecyclerViewAdapter
import io.fajarca.project.moviemate.data.factory.ItemTypeFactoryImpl
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.databinding.FragmentMoviesBinding
import io.fajarca.project.moviemate.domain.entity.ItemClickListener
import io.fajarca.project.moviemate.domain.entity.movielist.Movie
import io.fajarca.project.moviemate.helper.IdlingResourceWrapper
import io.fajarca.project.moviemate.helper.extension.gone
import io.fajarca.project.moviemate.helper.extension.loadBackdropImage
import io.fajarca.project.moviemate.helper.extension.visible

class MoviesFragment : BaseFragment<FragmentMoviesBinding, MoviesViewModel>(),
    ItemClickListener {

    override fun getLayoutResourceId() = R.layout.fragment_movies
    override fun getViewModelClass() = MoviesViewModel::class.java
    private val adapter by lazy { BaseRecyclerViewAdapter(ItemTypeFactoryImpl(), arrayListOf(), this) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        observeMoviesResult()
        observeBanner()
        vm.getMovies()
    }

    private fun observeMoviesResult() {
        vm.movies.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Loading -> {
                    IdlingResourceWrapper.increment()
                    binding.recyclerView.gone()
                    binding.progressBar.visible()
                }
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.visible()
                    adapter.refreshItems(it.data)
                }
                is Result.Error -> {
                    IdlingResourceWrapper.decrement()
                    binding.progressBar.gone()
                    binding.recyclerView.gone()
                }
            }
        })
    }

    private fun observeBanner() {
        vm.banner.observe(viewLifecycleOwner, Observer {
            when (it) {
                is Result.Success -> {
                    IdlingResourceWrapper.decrement()
                    binding.ivMovie.loadBackdropImage(it.data.imageUrl)
                    binding.tvTitle.text = it.data.title
                }
                is Result.Empty -> {
                    IdlingResourceWrapper.decrement()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    override fun onClick(item: BaseItemModel) {
        val movie = item as Movie
        findNavController().navigate(MoviesFragmentDirections.actionFragmentMoviesToFragmentMovieDetail(movie.id))
    }
}
