package io.fajarca.project.moviemate.domain.usecase.tvserieslist

import io.fajarca.project.moviemate.abstraction.UseCase
import io.fajarca.project.moviemate.data.vo.Result
import io.fajarca.project.moviemate.domain.entity.serieslist.Series
import io.fajarca.project.moviemate.domain.repository.SeriesRepository
import javax.inject.Inject

class GetOnTheAirSeriesUseCase @Inject constructor(private val repository: SeriesRepository) :
    UseCase<List<Series>, UseCase.None>() {

    override suspend operator fun invoke(params: None): List<Series> {
        val result = repository.getOnTheAirSeries()
        return when (result) {
            is Result.Success -> result.data
            else -> emptyList()
        }
    }
}
