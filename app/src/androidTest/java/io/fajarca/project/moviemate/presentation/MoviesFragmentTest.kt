package io.fajarca.project.moviemate.presentation

import android.content.Intent
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import io.fajarca.project.moviemate.R
import io.fajarca.project.moviemate.dispatcher.MockWebServerDispatcher
import io.fajarca.project.moviemate.presentation.screen.movies.MoviesFragment
import io.fajarca.project.moviemate.rule.IdlingResourceRule
import io.fajarca.project.moviemate.rule.MockWebServerRule
import org.hamcrest.core.IsNot.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MoviesFragmentTest {

    @get:Rule
    val idlingResourceRule = IdlingResourceRule()

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    @get:Rule
    val mockWebServerRule = MockWebServerRule()

    @Test
    fun testRecyclerShouldVisibleWhenGetFromRemoteSuccess() {
        mockWebServerRule.server.dispatcher = MockWebServerDispatcher().SuccessDispatcher(200)
        activityRule.launchActivity(Intent())

        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        ).apply {
            setGraph(R.navigation.nav_main)
            setCurrentDestination(R.id.fragmentHome)
        }

        val scenario = launchFragmentInContainer<MoviesFragment>()
        scenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
        }

        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
    }

    @Test
    fun testRecyclerShouldInvisibleWhenGetFromRemoteFailed() {
        mockWebServerRule.server.dispatcher = MockWebServerDispatcher().ErrorDispatcher(404)
        activityRule.launchActivity(Intent())

        val navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        ).apply {
            setGraph(R.navigation.nav_main)
            setCurrentDestination(R.id.fragmentHome)
        }

        val scenario = launchFragmentInContainer<MoviesFragment>()
        scenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
        }

        onView(withId(R.id.recyclerView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())))
    }
}
