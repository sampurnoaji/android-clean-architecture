package io.fajarca.project.moviemate

import io.fajarca.project.moviemate.di.TestNetworkModule
import io.fajarca.project.moviemate.di.component.AppComponent
import io.fajarca.project.moviemate.di.component.DaggerAppComponent
import io.fajarca.project.moviemate.di.modules.ContextModule

class UiTestMovieMateApp : MovieMateApp() {
    override fun onCreate() {
        super.onCreate()
        getComponent().inject(this)
    }
    override fun getComponent(): AppComponent {
        return DaggerAppComponent
            .builder()
            .contextModule(ContextModule(this))
            .networkModule(TestNetworkModule())
            .build()
    }
}
