package io.fajarca.project.moviemate.dispatcher

import android.net.Uri
import io.fajarca.project.moviemate.helper.FileUtils
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest

class MockWebServerDispatcher {

    inner class SuccessDispatcher(private val responseCode: Int) : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            val url = request.path ?: return MockResponse().setResponseCode(404)
            val endpoint = Uri.parse(url).lastPathSegment
            return MockResponse()
                .setResponseCode(responseCode)
                .setBody(FileUtils.readTestResourceFile("json/$endpoint.json"))
        }
    }

    inner class ErrorDispatcher(private val responseCode: Int) : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return MockResponse()
                .setResponseCode(responseCode)
        }
    }
}
